﻿using System;
using System.Linq;

namespace ORM_Charniauski
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var scope = new NorthwindDb())
            {
                var products = scope.Products.Where(p => p.Category != null && p.Supplier != null).ToList();
                var allProduct = scope.Products.ToList();

                Console.WriteLine("Count of Products with not null Supplier and Category : {0}", products.Count);
                Console.WriteLine("All products : {0}", allProduct.Count);
            }

            Console.ReadLine();
        }
    }
}
