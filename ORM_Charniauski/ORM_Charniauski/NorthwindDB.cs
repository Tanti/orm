﻿using LinqToDB;
using LinqToDB.Data;
using ORM_Charniauski.Models;

namespace ORM_Charniauski
{
    public class NorthwindDb : DataConnection
    {
        public NorthwindDb()
            : base("Northwind")
        {

        }

        public ITable<Product> Products { get { return GetTable<Product>(); } }
        public ITable<Category> Categories { get { return GetTable<Category>(); } }
        public ITable<Supplier> Suppliers { get { return GetTable<Supplier>(); } }
    }
}
