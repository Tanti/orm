﻿using LinqToDB.Mapping;

namespace ORM_Charniauski.Models
{
    [Table(Name = "Categories")]
    public class Category
    {
        [PrimaryKey, Identity]
        [Column(Name = "CategoryID")]
        public int Id { get; set; }
        [Column]
        public string CategoryName { get; set; }
        [Column]
        public string Description { get; set; }
        [Column]
        public byte[] Picture { get; set; }
    }
}
