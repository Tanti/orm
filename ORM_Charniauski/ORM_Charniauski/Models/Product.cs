﻿using System.Collections.Generic;
using LinqToDB.Mapping;

namespace ORM_Charniauski.Models
{
    [Table(Name = "Products")]
    public class Product
    {
        [PrimaryKey, Identity]
        [Column(Name = "ProductID")]
        public int Id { get; set; }

        [Column]
        public string ProductName { get; set; }

        [Association(ThisKey = "SupplierID", OtherKey = "Id", CanBeNull = true)]
        public Supplier Supplier { get; set; }
        [Column]
        public int? SupplierID { get; set; }

        [Association(ThisKey = "CategoryID", OtherKey = "Id", CanBeNull = true)]
        public Category Category { get; set; }
        [Column]
        public int? CategoryID { get; set; }

        [Column]
        public string QuantityPerUnit { get; set; }
        [Column]
        public decimal? UnitPrice { get; set; }
        [Column]
        public int UnitsInStock { get; set; }
        [Column]
        public int UnitsOnOrder { get; set; }
        [Column]
        public int ReorderLevel { get; set; }
        [Column]
        public int Discontinued { get; set; }
    }
}
